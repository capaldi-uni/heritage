#ifndef DATABASE_H
#define DATABASE_H

#include "../record.h"
#include <QHash>
#include <QMap>
#include <QVector>
#include <windows.h>

class Database
{
public:
    Database();
    ~Database();

    int count() const;                      //returns count of records in database
    int append(Record &record);             //appends database with record and returns position of record in sorted array
    void remove(uint id);                   //removes record from database
    int update(const Record &record);       //updates content of given record and returns position of record in sorted array
    bool record(uint id, Record &record) const;     //puts record with given id in record, if no such record, 0 returned
    const QVector<recLine> records() const; //returns vector of lines to be displayed in database browser
    bool save() const;                      //saves database in given file
    bool load();                            //loads database from given file
    void clear();                           //clear database
    bool isModified() const;                //returns sign of database was modified after load/save


    int getLongestName() const;             //returns length of longest name
    uint idOf(QString name) const;          //returns id of record with given name
    bool isParent(uint id) const;           //checks if record with given id is a parent
    dateRanges getDateRanges(const Record &state) const;        //returns struct of date ranges for record state
    QVector<QString> getParentsList(const Record &state) const; //returns vector of avialable parents

    QVector<uint> getChildrenList(uint id) const;   //returns vector of children's ids
    int getSortPos(uint id) const;                  //returns position of record in sorted array

    bool ok;

    unsigned short int port = 55555;    //port with default value

private:    
    bool loadConfig();                  //loading configurations from file

    bool grabMutex() const;                   //waiting and claiming mutex
    void releaseMutex() const;                //releasing mutex

    QHash<uint, Record> db;             //array of records, key - id
    mutable bool isMod;                 //sign of database was modified
    uint freeID;                        //id to assign to new record
    TCHAR filename[100] = _T("database.htg");

    HANDLE hEdit;
};

#endif // DATABASE_H
