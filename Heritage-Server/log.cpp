#include "log.h"

bool createlog()  //creates log file and assigns it's handle to standard error stream handle
{
    //get time at the very begining
    SYSTEMTIME time;
    GetLocalTime(&time);

    TCHAR logname[50];  //log file name

    //inserting time in filename
    _stprintf(logname, _T(".\\log\\%d-%02d-%02d-%02d-%02d-%02d-log.txt"),
              time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);

    //creating directory if needed
    DWORD dir = GetFileAttributes(_T("log"));
    if(dir == INVALID_FILE_ATTRIBUTES || !(dir & FILE_ATTRIBUTE_DIRECTORY))
        CreateDirectory(_T("log"), nullptr);

    //creating file
    HANDLE log = CreateFile(logname, GENERIC_WRITE, FILE_SHARE_READ, nullptr, CREATE_ALWAYS, 0, nullptr);
    if(log != INVALID_HANDLE_VALUE)
    {
        SetStdHandle(STD_ERROR_HANDLE, log);    //redirecting standard error stream to created file
        return 1;
    }

    return 0;
}

void log(const TCHAR *message)  //puts message in error stream preppending it with current date and time
{
    //get time at the very begining
    SYSTEMTIME time;
    GetLocalTime(&time);

    //get error stream handle
    HANDLE error = GetStdHandle(STD_ERROR_HANDLE);

    TCHAR tmessage[30]; //time to prepend message
    _stprintf(tmessage, _T("%d.%02d.%02d-%02d:%02d:%02d: "),
              time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);

    DWORD bw;
    WriteFile(error, tmessage, DWORD(_tcslen(tmessage) * sizeof(TCHAR)), &bw, nullptr); //putting time
    WriteFile(error, message, DWORD(_tcslen(message) * sizeof(TCHAR)), &bw, nullptr);   //putting message
    WriteFile(error, _T("\n"), sizeof(TCHAR), &bw, nullptr);    //new line

}

void logLocal(const TCHAR *message, int id) //puts message in error stream preppending it with thread id and current date and time
{
    //get time at the very begining
    SYSTEMTIME time;
    GetLocalTime(&time);

    //get error stream handle
    HANDLE error = GetStdHandle(STD_ERROR_HANDLE);

    TCHAR tmessage[30]; //time to prepend message
    _stprintf(tmessage, _T("%d.%02d.%02d-%02d:%02d:%02d: "),
              time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);

    TCHAR idmessage[20];    //prepending id
    _stprintf(idmessage, _T("Local %d: "), id);

    DWORD bw;
    WriteFile(error, tmessage, DWORD(_tcslen(tmessage) * sizeof(TCHAR)), &bw, nullptr); //putting time
    WriteFile(error, idmessage, DWORD(_tcslen(idmessage) * sizeof(TCHAR)), &bw, nullptr); //putting time
    WriteFile(error, message, DWORD(_tcslen(message) * sizeof(TCHAR)), &bw, nullptr);   //putting message
    WriteFile(error, _T("\n"), sizeof(TCHAR), &bw, nullptr);    //new line
}

void logRemote(const TCHAR *message, const char* ip)   //puts message in error stream preppending it with client ip and current date and time
{
    //get time at the very begining
    SYSTEMTIME time;
    GetLocalTime(&time);

    //get error stream handle
    HANDLE error = GetStdHandle(STD_ERROR_HANDLE);

    TCHAR tmessage[30]; //time to prepend message
    _stprintf(tmessage, _T("%d.%02d.%02d-%02d:%02d:%02d: "),
              time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);

    TCHAR idmessage[40];    //prepending id
    _stprintf(idmessage, _T("Remote %s: "), ip);

    DWORD bw;
    WriteFile(error, tmessage, DWORD(_tcslen(tmessage) * sizeof(TCHAR)), &bw, nullptr); //putting time
    WriteFile(error, idmessage, DWORD(_tcslen(idmessage) * sizeof(TCHAR)), &bw, nullptr); //putting time
    WriteFile(error, message, DWORD(_tcslen(message) * sizeof(TCHAR)), &bw, nullptr);   //putting message
    WriteFile(error, _T("\n"), sizeof(TCHAR), &bw, nullptr);    //new line
}
