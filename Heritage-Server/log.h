#ifndef LOG_H
#define LOG_H

#include <windows.h>
#include <tchar.h>

bool createlog();  //creates log file and assigns it's handle to standard error stream handle

void log(const TCHAR *message);  //puts message in error stream preppending it with current date and time

void logLocal(const TCHAR *message, int id);   //puts message in error stream preppending it with thread id and current date and time

void logRemote(const TCHAR *message, const char* ip);   //puts message in error stream preppending it with client ip and current date and time

#endif // LOG_H
