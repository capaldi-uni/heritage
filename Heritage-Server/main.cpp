#include <QCoreApplication>
#include "server.h"


int main(int argc, char *argv[])
{
    WORD ver = MAKEWORD(2,2);
    WSADATA data;
    int err;
    err = WSAStartup(ver, &data);
    if(err)
    {
        log(_T("Unable to initialize socket system"));
        std::cout << "Unable to initialize socket system\nTCP connection will not be avialible" << std::endl;
    }

    HANDLE hSingleInstance = CreateMutex(nullptr, true, _T("HeritageServerSingle"));
    if(GetLastError() != ERROR_ALREADY_EXISTS)
    {
        QCoreApplication a(argc, argv);
        createlog();

        Server *sv = new Server(nullptr, &a);

        int res = a.exec();
        delete sv;

        ReleaseMutex(hSingleInstance);
        CloseHandle(hSingleInstance);
        return res;
    }

    WSACleanup();

    std::cout << "Server is already running" << std::endl;
    Sleep(5000);
    return 0;
}
