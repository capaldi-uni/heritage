#include "server.h"
#include "threads.h"


Server::Server(QObject *parent, QCoreApplication *a)
    : QObject(parent)
{
    log(_T("Server started"));
    std::cout << "Server started" << std::endl;

    db = new Database();
    app = a;    //'a' should be pointer to QCoreApplication which provides event loop for this programm

    //timer initialization
    autosaver = new QTimer();
    connect(autosaver, SIGNAL(timeout()), this, SLOT(autosave()));

    bool ok = db->ok;   //check if database initialized successfully

    if(ok)
    {
        log(_T("Database initialized"));
        std::cout << "Database initialized successfully!" << std::endl;

        //creating user-connecting threads
        hPConnector = CreateThread(nullptr, 0, connectorPipeThread, (LPVOID)this, 0, nullptr);
        hSConnector = CreateThread(nullptr, 0, connectorSocketThread, (LPVOID)this, 0, nullptr);


        if(hPConnector == INVALID_HANDLE_VALUE) //if pipe connector not created, server can not be shutdown, so server cant continue work
        {
            log(_T("Error creating connecting threads"));
            std::cout << "Can not create connecting thread" << std::endl;

            QTimer::singleShot(0, app, SLOT(quit()));   //exit
        }
        else if(hSConnector == INVALID_HANDLE_VALUE)    //if socket connector not created, server still can work, but only with pipe clients
        {
            log(_T("Error creating socket connectpr thread"));
            std::cout << "Can not create socket connecting thread" << std::endl;

            std::cout << "Do you want to continue anyway? TCP connection will not be avialibele.\n(y/n)" << std::endl;
            char c;
            std::cin >> c;
            if(c != 'y')
                QTimer::singleShot(0, app, SLOT(quit()));   //exit
        }

    }
    else
    {
        log(_T("Error during database initialization"));
        std::cout << "Can not initialize database" << std::endl;

        QTimer::singleShot(0, app, SLOT(quit()));   //exit
    }

    InitializeCriticalSection(&sUpdate);

    autosaver->start(60000);    //autosave every minute
    createTrayIcon();
    showConsole(0);     //hide console
}

Server::~Server()
{
    exiting = true;

    log(_T("Shutdown"));
    std::cout << "Exiting..." << std::endl;

    delete db;

    Shell_NotifyIcon(NIM_DELETE, data); //delete tray icon
    showConsole(1);

    DeleteCriticalSection(&sUpdate);

    if(hPConnector != INVALID_HANDLE_VALUE)
    {
        TerminateThread(hPConnector, 0);
        CloseHandle(hPConnector);
        Sleep(5000);
    }

    delete autosaver;
}

void Server::exit()
{
    exiting = true;
    app->quit();
}

//set last updater id
void Server::update(int updater)
{
    EnterCriticalSection(&sUpdate);
        lastUpdater = updater;
    LeaveCriticalSection(&sUpdate);
}

//returns port for socket
unsigned short int Server::port()
{
    return db->port;
}

//hide and show console window
void Server::showConsole(bool b)
{
    visible = b;
    ShowWindow(GetConsoleWindow(), b * 5);
}

//saves db
void Server::autosave()
{
    if(db->save())
    {
        std::cout << "Autosave successful!" << std::endl;
        log(_T("Autosave successful"));
    }
    else
    {
        bool v = visible;
        showConsole(1);
        std::cout << "Autosave unsuccessful!" << std::endl;
        log(_T("Autosave unsuccessful"));
        if(!v)  //hide console if it was hidden
        {
            Sleep(5000);
            showConsole(0);
        }
    }
}

//creates server icon in system tray (just displaying that server is here)
void Server::createTrayIcon()
{
    //set icon properties
    data = new NOTIFYICONDATA{};
    data->uID = 1234;
    data->hWnd = GetConsoleWindow();
    data->uFlags = NIF_TIP | NIF_ICON;
    data->hIcon = LoadIcon(nullptr, IDI_APPLICATION);
    _tcscpy(data->szTip, _T("Сервер запущен"));

    Shell_NotifyIcon(NIM_ADD,  data);   //display icon
}

