#include "serverslave.h"
#include "../pipeworks.h"


ServerSlave::ServerSlave(Server* master, int id)
{
    server = master;
    slaveId = id;
};

ServerSlave::~ServerSlave() { }

int ServerSlave::id() { return slaveId; }

const char* ServerSlave::ip() { return nullptr; }


PipeSlave::PipeSlave(Server *master, int id, HANDLE hClientPipe)
    : ServerSlave(master, id)
{
    hPipe = hClientPipe;

    bool ok = true;

    writePipe(hPipe, &ok, sizeof(ok));

    logLocal(_T("Thread initialized"), slaveId);
    std::cout << "Thread " << slaveId << " initialized" << std::endl;
}

void PipeSlave::process()
{
    request req;
    Record rec;

    int pos;
    int error;
    uint id;

    //trying to read request
    error = readPipe(hPipe, &req, sizeof (req));

    if(!error)  //processing request
    {
        //set pipe in blocking mode to avoid data loosing
        DWORD mode = PIPE_WAIT;
        SetNamedPipeHandleState(hPipe, &mode, nullptr, nullptr);

        switch (req)    //process request
        {
        case request::count:
        {
            int count = server->db->count();
            error = writePipe(hPipe, &count, sizeof (count));  //count
            break;
        }
        case request::append:
        {
            error = readPipe(hPipe, &rec, sizeof (rec));  //new record
            if(error) break;

            pos = server->db->append(rec);
            server->update(slaveId);
            error = writePipe(hPipe, &pos, sizeof (pos));  //sort position
            if(error) break;

            error = writePipe(hPipe, &rec.id, sizeof (rec.id));    //id of new record
            break;
        }
        case request::remove:
        {
            error = readPipe(hPipe, &id, sizeof (id));
            if(error) break;
            server->db->remove(id);
            server->update(slaveId);
            break;
        }
        case request::update:
        {
            error = readPipe(hPipe, &rec, sizeof (rec));  //record to be updated
            if(error) break;

            pos = server->db->update(rec);
            server->update(slaveId);
            error = writePipe(hPipe, &pos, sizeof (pos));  //sort position
            break;
        }
        case request::record:
        {
            error = readPipe(hPipe, &id, sizeof (id));
            if(error) break;

            bool is = server->db->record(id, rec);
            error = writePipe(hPipe, &is, sizeof (is));
            if(error) break;

            if(is)
                error = writePipe(hPipe, &rec, sizeof (rec));
            break;
        }
        case request::records:
        {
            QVector<recLine> lines = server->db->records();
            int count = lines.count();
            error = writePipe(hPipe, &count, sizeof (count));  //number of lines
            if(error) break;

            QVectorIterator<recLine> iter(lines);
            while(iter.hasNext())   //lines one by one
            {
                recLine line = iter.next();
                error = writePipe(hPipe, &line, sizeof (line));
                if(error) break;
            }
            break;
        }
        case request::clear:
        {
            server->db->clear();
            server->update(slaveId);
            break;
        }
        case request::mod:
        {
            bool isMod = slaveId != server->lastUpdater;
            error = writePipe(hPipe, &isMod, sizeof (isMod));
            break;
        }
        case request::longest:
        {
            int len = server->db->getLongestName();
            error = writePipe(hPipe, &len, sizeof (len));  //longest length
            break;
        }
        case request::idof:
        {
            QString name;
            readQString(hPipe, &name);
            id = server->db->idOf(name);
            error = writePipe(hPipe, &id, sizeof (id));    //return id
            break;
        }
        case request::isparent:
        {
            error = readPipe(hPipe, &id, sizeof (id));
            if(error) break;

            bool par = server->db->isParent(id);
            error = writePipe(hPipe, &par, sizeof (par));
            break;
        }
        case request::get_ranges:
        {
            error = readPipe(hPipe, &rec, sizeof (rec));
            if(error) break;

            dateRanges rngs = server->db->getDateRanges(rec);
            error = writePipe(hPipe, &rngs, sizeof (rngs));
            break;
        }
        case request::get_parents:
        {
            error = readPipe(hPipe, &rec, sizeof (rec));
            if(error) break;

            QVector<QString> parents;

            parents = server->db->getParentsList(rec);
            int count = parents.count();

            error = writePipe(hPipe, &count, sizeof (count));  //count of strings
            if(error) break;

            if(count > 1)   //if there is parents
            {
                int len;
                QVectorIterator<QString> iter(parents);
                while(iter.hasNext())
                {
                    QString pr = iter.next();
                    len = pr.length();
                    error = writePipe(hPipe, &len, sizeof (len));  //length of string
                    if(error) break;

                    if(len)    //don't try to write empty string - separator
                    {
                        error = writePipe(hPipe, pr.data(), DWORD(len) * sizeof (QChar)); //string with name
                        if(error) break;
                    }
                    //writeQString is not used here due to need of this check
                }
            }
            break;
        }
        case request::get_children:
        {
            error = readPipe(hPipe, &id, sizeof (id));
            if(error) break;

            QVector<uint> children = server->db->getChildrenList(id);
            int count = children.count();
            error = writePipe(hPipe, &count, sizeof (count));  //count of children
            if(error) break;

            QVectorIterator<uint> iter(children);
            while(iter.hasNext())   //id's one by one
            {
                uint ch = iter.next();
                error = writePipe(hPipe, &ch, sizeof (ch));
                if(error) break;
            }
            break;
        }
        case request::get_pos:
        {
            error = readPipe(hPipe, &id, sizeof (id));
            if(error) break;

            pos = server->db->getSortPos(id);
            error = writePipe(hPipe, &pos, sizeof (pos));

            break;
        }
        case request::exit:
        {
            logLocal(_T("Client disconnected"), slaveId);
            std::cout << "Thread " << slaveId << ": Client disconnected" << std::endl;
            disconnected = 1;
        }
        }

        if(error > 0)
        {
            if(error == 1)
            {
                logLocal(_T("Connection lost"), slaveId);
                std::cout << "Thread " << slaveId << ": Connection lost" << std::endl;
            }
            else
            {
                logLocal(_T("Unknown error"), slaveId);
                std::cout << "Thread " << slaveId << ": Unknown error occured" << std::endl;
            }

            disconnected = 1;

        }

        //returning back to nonblocking mode
        mode = PIPE_NOWAIT;
        SetNamedPipeHandleState(hPipe, &mode, nullptr, nullptr);
    }
}


SocketSlave::SocketSlave(Server *master, sockaddr_in sain, int id, SOCKET scClientSocket)
    : ServerSlave(master, id)
{
    scClient = scClientSocket;

    //conver ip addres to string
    bool ok = true;
    char* ipa = inet_ntoa(sain.sin_addr);
    strcpy(ip_addr, ipa);

    sendSock(scClient, (char*)&ok, sizeof(ok));

    logRemote(_T("Thread initialized"), ip_addr);
    std::cout << "Client " << ip_addr << ": thread initialized" << std::endl;
}

void SocketSlave::process()
{
    request req;
    Record rec;

    int pos;
    bool ok;
    uint id;

    //read request
    ok = recvSock(scClient, (char*)&req, sizeof (req));

    if(ok)  //processing request
    {
        switch (req)    //process request
        {
        case request::count:
        {
            int count = server->db->count();
            ok = sendSock(scClient, (char*)&count, sizeof (count));  //count
            break;
        }
        case request::append:
        {
            ok = recvSock(scClient, (char*)&rec, sizeof (rec));  //new record
            if(!ok) break;

            pos = server->db->append(rec);
            server->update(slaveId);
            ok = sendSock(scClient, (char*)&pos, sizeof (pos));  //sort position
            if(!ok) break;

            ok = sendSock(scClient, (char*)&rec.id, sizeof (rec.id));    //id of new record
            break;
        }
        case request::remove:
        {
            ok = recvSock(scClient, (char*)&id, sizeof (id));
            if(!ok) break;
            server->db->remove(id);
            server->update(slaveId);
            break;
        }
        case request::update:
        {
            ok = recvSock(scClient, (char*)&rec, sizeof (rec));  //record to be updated
            if(!ok) break;

            pos = server->db->update(rec);
            server->update(slaveId);
            ok = sendSock(scClient, (char*)&pos, sizeof (pos));  //sort position
            break;
        }
        case request::record:
        {
            ok = recvSock(scClient, (char*)&id, sizeof (id));
            if(!ok) break;

            bool is = server->db->record(id, rec);
            ok = sendSock(scClient, (char*)&is, sizeof (is));
            if(!ok) break;

            if(is)
                ok = sendSock(scClient, (char*)&rec, sizeof (rec));
            break;
        }
        case request::records:
        {
            QVector<recLine> lines = server->db->records();
            int count = lines.count();
            ok = sendSock(scClient, (char*)&count, sizeof (count));  //number of lines
            if(!ok) break;

            QVectorIterator<recLine> iter(lines);
            while(iter.hasNext())   //lines one by one
            {
                recLine line = iter.next();
                ok = sendSock(scClient, (char*)&line, sizeof (line));
                if(!ok) break;
            }
            break;
        }
        case request::clear:
        {
            server->db->clear();
            server->update(slaveId);
            break;
        }
        case request::mod:
        {
            bool isMod = slaveId != server->lastUpdater;
            ok = sendSock(scClient, (char*)&isMod, sizeof (isMod));
            break;
        }
        case request::longest:
        {
            int len = server->db->getLongestName();
            ok = sendSock(scClient, (char*)&len, sizeof (len));  //longest length
            break;
        }
        case request::idof:
        {
            QString name;
            recvQString(scClient, &name);
            id = server->db->idOf(name);
            ok = sendSock(scClient, (char*)&id, sizeof (id));    //return id
            break;
        }
        case request::isparent:
        {
            ok = recvSock(scClient, (char*)&id, sizeof (id));
            if(!ok) break;

            bool par = server->db->isParent(id);
            ok = sendSock(scClient, (char*)&par, sizeof (par));
            break;
        }
        case request::get_ranges:
        {
            ok = recvSock(scClient, (char*)&rec, sizeof (rec));
            if(!ok) break;

            dateRanges rngs = server->db->getDateRanges(rec);
            ok = sendSock(scClient, (char*)&rngs, sizeof (rngs));
            break;
        }
        case request::get_parents:
        {
            ok = recvSock(scClient, (char*)&rec, sizeof (rec));
            if(!ok) break;

            QVector<QString> parents;

            parents = server->db->getParentsList(rec);
            int count = parents.count();

            ok = sendSock(scClient, (char*)&count, sizeof (count));  //count of strings
            if(!ok) break;

            if(count > 1)   //if there is parents
            {
                int len;
                QVectorIterator<QString> iter(parents);
                while(iter.hasNext())
                {
                    QString pr = iter.next();
                    len = pr.length();
                    ok = sendSock(scClient, (char*)&len, sizeof (len));  //length of string
                    if(!ok) break;

                    if(len)    //don't try to write empty string - separator
                    {
                        ok = sendSock(scClient, (char*)pr.data(), len * sizeof (QChar)); //string with name
                        if(!ok) break;
                    }
                    //writeQString is not used here due to need of this check
                }
            }
            break;
        }
        case request::get_children:
        {
            ok = recvSock(scClient, (char*)&id, sizeof (id));
            if(!ok) break;

            QVector<uint> children = server->db->getChildrenList(id);
            int count = children.count();
            ok = sendSock(scClient, (char*)&count, sizeof (count));  //count of children
            if(!ok) break;

            QVectorIterator<uint> iter(children);
            while(iter.hasNext())   //id's one by one
            {
                uint ch = iter.next();
                ok = sendSock(scClient, (char*)&ch, sizeof (ch));
                if(!ok) break;
            }
            break;
        }
        case request::get_pos:
        {
            ok = recvSock(scClient, (char*)&id, sizeof (id));
            if(!ok) break;

            pos = server->db->getSortPos(id);
            ok = sendSock(scClient, (char*)&pos, sizeof (pos));

            break;
        }
        case request::exit:
        {
            logRemote(_T("Client disconnected"), ip_addr);
            std::cout << "Client " << ip_addr << ": Client disconnected" << std::endl;
            disconnected = 1;
        }
        }
    }

    if(!ok)
    {

        logRemote(_T("Connection lost"), ip_addr);
        std::cout << "Client " << ip_addr << ": Connection lost" << std::endl;

        disconnected = 1;
    }
}

const char* SocketSlave::ip() { return ip_addr; }
