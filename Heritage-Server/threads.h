#ifndef THREADS_H
#define THREADS_H

#include "../pipeworks.h"
#include "server.h"
#include "serverslave.h"

#define TYPE_PIPE 0
#define TYPE_SOCKET 1

//structure with slaveThread initialization data
struct slaveInit
{
    Server* server; //parent server
    int type;       //type of slave
    int id;         //thread id
    union{
        HANDLE pipe;    //pipe connected to client
        SOCKET socket;  //socket connected to client
    } chanel;
    sockaddr_in sin;
};

//thread to handle client
DWORD WINAPI slaveThread(LPVOID init)
{
    slaveInit *i = (slaveInit*)init;
    Server *s = i->server;
    int type = i->type;

    //creating slave to process client requests
    ServerSlave *slave;     //polimorphism goes brrr
    if(type == TYPE_PIPE)
        slave = new PipeSlave(i->server, i->id, i->chanel.pipe);
    else
        slave = new SocketSlave(i->server, i->sin, i->id, i->chanel.socket);

    delete i;

    while(!s->exiting)  //until recieve exit signal
    {
        slave->process();    //process request if there is
        if(slave->disconnected)  //exit if disconnected
            break;
        Sleep(1);
    }

    if(type == TYPE_PIPE)
    {
        logLocal(_T("Terminating thread"), slave->id());
        std::cout << "Thread " << slave->id() << ": Terminating thread" << std::endl;
    }
    else
    {
        logRemote(_T("Terminating thread"), slave->ip());
        std::cout << "Client " << slave->ip() << ": Terminating thread" << std::endl;
    }

    delete slave;

    return 0;
}

//thread to connect clients and create handling threads(using pipes)
DWORD WINAPI connectorPipeThread(LPVOID init)
{
    Server* s = (Server*)init;  //pointer to server
    int id = 1; //id of new thread/client
    log(_T("Pipe connector thread initialized"));
    std::cout << "Pipe connection oppened" << std::endl;

    while(true)
    {
        //creating pipe
        HANDLE hPipe = CreateNamedPipe(_T("\\\\.\\pipe\\heritage"), PIPE_ACCESS_DUPLEX, 0,
                                       PIPE_UNLIMITED_INSTANCES, 4096, 4096, 0, nullptr);

        if(hPipe != INVALID_HANDLE_VALUE)
        {
            log(_T("Pipe created"));
            std::cout << "New pipe created" << std::endl;

            if(ConnectNamedPipe(hPipe, nullptr))    //awaiting client to connect
            {
                int type;
                readPipe(hPipe, &type, sizeof(type));   //getting type of client

                if(type)   //ordinary client
                {
                    log(_T("Pipe connectd"));
                    std::cout << "Client connected succesfully\nLaunching new thread..." << std::endl;

                    unsigned long mode = PIPE_NOWAIT;
                    SetNamedPipeHandleState(hPipe, &mode, nullptr, nullptr);    //set pipe to non-blocking mode

                    slaveInit *init = new slaveInit{};
                    init->server = s;
                    init->type = TYPE_PIPE;
                    init->id = id;
                    init->chanel.pipe = hPipe;

                    CreateThread(nullptr, 0, slaveThread, (LPVOID)init, 0, nullptr);    //launch new thread to handle client

                    id++;
                    hPipe = INVALID_HANDLE_VALUE;
                }
                else    //exit client
                {
                    log(_T("Exit client connectd"));
                    std::cout << "Exit client connected" << std::endl;
                    DisconnectNamedPipe(hPipe);
                    CloseHandle(hPipe);

                    s->exit();  //request exit
                    break;
                }
            }
        }
    }

    return 0;
}

//thread to connect clients and create handling threads(using sockets)
DWORD WINAPI connectorSocketThread(LPVOID init)
{
    int pc = 30;
    Server* s = (Server*)init;  //pointer to server
    int id = 100;

    log(_T("Socket connector thread initialized"));
    std::cout << "Socket connection thread initialized" << std::endl;

    SOCKET sc, scclient;

    //задаём структуру настройки сокетов
    sockaddr_in sain = {}, sainclient;
    sain.sin_family = AF_INET;
    sain.sin_port = htons(s->port());
    sain.sin_addr.s_addr = htonl(INADDR_ANY);
    int size = sizeof(sockaddr_in);

    //создаём сокет для приёма клиентов
    sc = socket(AF_INET, SOCK_STREAM, 0);

    if(sc != INVALID_SOCKET)
    {
        TCHAR msg[40] = {};
        _stprintf(msg, _T("TCP connection oppened on port %d"), s->port());
        log(msg);
        std::cout << "TCP connection opened on port " << s->port() << std::endl;

        bind(sc, (sockaddr*)&sain, size);
        listen(sc, pc); //переводим сокет в пасивный режим

        while(true)
        {
            //подключаеми клиента
            scclient = accept(sc, (sockaddr*)&sainclient, &size);

            if(scclient != INVALID_SOCKET)
            {
                slaveInit *init = new slaveInit{};
                init->server = s;
                init->type = TYPE_SOCKET;
                init->id = id;
                init->chanel.socket = scclient;
                init->sin = sainclient;

                CreateThread(nullptr, 0, slaveThread, init, 0, nullptr);  //создаём поток для работы с клиентом
                Sleep(100);

                id++;
            }
        }
    }

    log(_T("Unable to create socket"));
    std::cout << "Unable to create socket" << std::endl;

    return 0;
}
#endif // THREADS_H
