#include "database.h"
#include "pipeworks.h"
#include <math.h>

Database::Database(const char* addr, unsigned short int port)
{
    //socket system init
    WORD ver = MAKEWORD(2,2);
    WSADATA data;
    int err;
    err = WSAStartup(ver, &data);
    if(!err)
    {
        bool res;
        int error;

        while(true)
        {
            //creating socket
            sc = socket(AF_INET, SOCK_STREAM, 0);
            if(sc != INVALID_SOCKET) //if successful
            {
                //socket init data
                sockaddr_in sain;
                sain.sin_family = AF_INET;
                sain.sin_port = htons(port);
                sain.sin_addr.s_addr = inet_addr(addr);

                //attempting to connect
                if (!connect(sc, (sockaddr*)&sain, sizeof(sockaddr_in)))
                {
                    bool ok = recvSock(sc, (char*)&res, sizeof(res));   //get status (wether server ok)

                    if(ok && res)
                        opened = 0; //successful
                    else
                        opened = 1; //connected but error on server side

                    break;
                }
            }

            error = WSAGetLastError();

            if(error == WSAETIMEDOUT || error == WSAECONNREFUSED)
            {
                opened = 3; //no connection (no server)
                break;
            }

            opened = 2;     //other error
            break;
        }
    }
}

Database::~Database()
{
    WSACleanup();
}

int Database::status() const //returns status of database
{
    return opened;
}

int Database::count() const //returns count of records in database
{
    bool ok;
    request req = request::count;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok)
    {
        opened = 4;
        return -1;
    }

    int count;
    ok = recvSock(sc, (char*)&count, sizeof (count));
    if(!ok)
    {
        opened = 4;
        return -1;
    }

    return count;
}

int Database::append(Record &record) //appends database with record and returns position of record in sorted array
{
    bool ok;
    request req = request::append;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok)
    {
        opened = 4;
        return -1;
    }

    ok = sendSock(sc, (char*)&record, sizeof (record));
    if(!ok)
    {
        opened = 4;
        return -1;
    }

    int pos;
    ok = recvSock(sc, (char*)&pos, sizeof (pos));
    if(!ok)
    {
        opened = 4;
        return -1;
    }
    ok = recvSock(sc, (char*)&record.id, sizeof (record.id));
    if(!ok)
    {
        opened = 4;
        return -1;
    }
    return pos;
}

void Database::remove(unsigned int id) //removes record from database
{
    bool ok;
    request req = request::remove;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok)
    {
        opened = 4;
        return;
    }

    ok = sendSock(sc, (char*)&id, sizeof (id));
    if(!ok)
        opened = 4;
}

int Database::update(const Record &record) //updates content of given record and returns position of record in sorted array
{
    bool ok;
    request req = request::update;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok)
    {
        opened = 4;
        return -1;
    }

    int pos;
    ok = sendSock(sc, (char*)&record, sizeof (record));
    if(!ok)
    {
        opened = 4;
        return -1;
    }

    ok = recvSock(sc, (char*)&pos, sizeof (pos));
    if(!ok)
    {
        opened = 4;
        return -1;
    }
    return pos;
}

void Database::record(unsigned int id, Record &record) const //puts record with given id in record
{
    bool ok;
    request req = request::record;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok)
    {
        opened = 4;
        return;
    }

    ok = sendSock(sc, (char*)&id, sizeof (id));
    if(!ok)
    {
        opened = 4;
        return;
    }

    bool is = 0;
    ok = recvSock(sc, (char*)&is, sizeof (is));
    if(!ok)
        opened = 4;

    if(is)
    {
        ok = recvSock(sc, (char*)&record, sizeof (record));
        if(!ok)
            opened = 4;
    }

}

const QVector<recLine> Database::records() const //returns vector of lines to be displayed in database browser
{
    QVector <recLine> lines;
    recLine newLine;

    bool ok;
    request req = request::records;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok) opened = 4;

    int count;
    ok = recvSock(sc, (char*)&count, sizeof (count));
    if(!ok) opened = 4;

    for(int i = 0; i < count; i++)
    {
        ok = recvSock(sc, (char*)&newLine, sizeof (newLine));
        if(!ok)
        {
            opened = 4;
            break;
        }

        lines.append(newLine);
    }

    return lines;
}

void Database::clear() //clear database
{
    bool ok;
    request req = request::clear;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok) opened = 4;
}

bool Database::isModified() const //returns sign of database was modified after load/save
{
    bool ok;
    request req = request::mod;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok) opened = 4;

    bool mod = 0;
    ok = recvSock(sc, (char*)&mod, sizeof (mod));
    if(!ok) opened = 4;

    return mod;
}

int Database::getLongestName() const //returns length of longest name
{
    bool ok;
    request req = request::longest;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok) opened = 4;

    int len = -1;
    ok = recvSock(sc, (char*)&len, sizeof (len));
    if(!ok) opened = 4;

    return len;
}

uint Database::idOf(QString name) const //returns id of record with given name
{
    bool ok;
    request req = request::idof;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok) opened = 4;

    sendQString(sc, name);

    uint id;
    ok = recvSock(sc, (char*)&id, sizeof (id));
    if(!ok) opened = 4;

    return id;
}

bool Database::isParent(uint id) const //checks if record with given id is a parent
{
    bool ok;
    request req = request::isparent;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok)
    {
        opened = 4;
        return 0;
    }

    bool par = 0;
    ok = sendSock(sc, (char*)&id, sizeof (id));
    if(!ok) opened = 4;

    ok = recvSock(sc, (char*)&par, sizeof (par));
    if(!ok) opened = 4;

    return par;
}

dateRanges Database::getDateRanges(const Record &state) const //returns struct of date ranges for record state
{
    bool ok;
    request req = request::get_ranges;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok) opened = 4;

    dateRanges rngs;
    ok = sendSock(sc, (char*)&state, sizeof (state));
    if(!ok) opened = 4;

    ok = recvSock(sc, (char*)&rngs, sizeof (rngs));
    if(!ok) opened = 4;

    return rngs;
}

QVector<QString> Database::getParentsList(const Record &state) const //returns vector of avialable parents
{
    bool ok;
    request req = request::get_parents;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok) opened = 4;

    QVector<QString> parents;
    int count;
    int len;
    QString pr;

    ok = sendSock(sc, (char*)&state, sizeof (state));
    if(!ok)
    {
        opened = 4;
        return parents;
    }

    ok = recvSock(sc, (char*)&count, sizeof (count));
    if(!ok)
    {
        opened = 4;
        return parents;
    }

    if(count > 1)   //if there are parents
        for(int i = 0; i < count; i++)
        {
            ok = recvSock(sc, (char*)&len, sizeof (len));
            if(!ok)
            {
                opened = 4;
                break;
            }
            if(len) //if not separator
            {
                pr.resize(len);
                ok = recvSock(sc, (char*)pr.data(), len * sizeof (QChar));
            }
            else
                pr = "";

            parents.append(pr);
        }
    else
        parents.append(""); //no parents, only separator needed

    return parents;
}

QVector<uint> Database::getChildrenList(uint id) const //returns vector of children's ids
{
    bool ok;
    request req = request::get_children;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok) opened = 4;

    ok = sendSock(sc, (char*)&id, sizeof (id));
    if(!ok) opened = 4;

    QVector<uint> children;
    int count;
    uint ch;
    ok = recvSock(sc, (char*)&count, sizeof (count));
    if(!ok) opened = 4;

    for(int i = 0; i < count; i++)
    {
        ok = recvSock(sc, (char*)&ch, sizeof (ch));
        if(!ok)
        {
            opened = 4;
            break;
        }
        children.append(ch);
    }
    return children;
}

int Database::getSortPos(uint id) const //returns position of record in sorted array
{
    bool ok;
    request req = request::get_pos;
    ok = sendSock(sc, (char*)&req, sizeof (req));
    if(!ok)
    {
        opened = 4;
        return -1;
    }

    ok = sendSock(sc, (char*)&id, sizeof (id));
    if(!ok)
    {
        opened = 4;
        return -1;
    }

    int pos = -1;
    ok = recvSock(sc, (char*)&pos, sizeof (pos));
    if(!ok) opened = 4;

    return pos;
}

bool Database::exit() //exit
{
    request req = request::exit;
    sendSock(sc, (char*)&req, sizeof (req));

    closesocket(sc);
    return true;
}
