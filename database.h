#ifndef DATABASE_H
#define DATABASE_H

#include "record.h"
#include <QHash>
#include <QMap>
#include <QVector>
#include <windows.h>

class Database
{
public:
    Database(const char*, unsigned short int port);
    ~Database();

    int status() const;                     //returns status of database

    //all functions bellow should only be called if status returns 0
    int count() const;                      //returns count of records in database
    int append(Record &record);             //appends database with record and returns position of record in sorted array
    void remove(uint id);                   //removes record from database
    int update(const Record &record);       //updates content of given record and returns position of record in sorted array
    void record(uint id, Record &record) const;     //puts record with given id in record
    const QVector<recLine> records() const; //returns vector of lines to be displayed in database browser
    void clear();                           //clear database
    bool isModified() const;                //returns sign of database was modified after sync

    int getLongestName() const;             //returns length of longest name
    uint idOf(QString name) const;          //returns id of record with given name
    bool isParent(uint id) const;           //checks if record with given id is a parent
    dateRanges getDateRanges(const Record &state) const;        //returns struct of date ranges for record state
    QVector<QString> getParentsList(const Record &state) const; //returns vector of avialable parents

    QVector<uint> getChildrenList(uint id) const;   //returns vector of children's ids
    int getSortPos(uint id) const;                  //returns position of record in sorted array

    bool exit();                            //exit

private:    
    mutable int opened;     //status of database (opened or error)

    SOCKET sc;              //server socket
};

#endif // DATABASE_H
