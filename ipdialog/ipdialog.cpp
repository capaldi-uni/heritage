#include "ipdialog.h"
#include "ui_ipdialog.h"

IPdialog::IPdialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IPdialog)
{
    ui->setupUi(this);

    ui->ipEdit->setText(defaultIP);
    setDefault();

    ival = new IpValidator(this);
    pval = new PortValidator(this);

    ui->ipEdit->setValidator(ival);
    ui->portEdit->setValidator(pval);

    connect(ui->defButton, SIGNAL(clicked()), this, SLOT(setDefault()));
}

IPdialog::~IPdialog()
{
    delete ival;
    delete pval;
    delete ui;
}

QStringList IPdialog::getIP() const
{
    QStringList list;
    list.append("0");

    int flag = 0;

    if(ui->ipEdit->hasAcceptableInput())
        list.append(this->ui->ipEdit->text());
    else
        flag += 1;

    if(ui->portEdit->hasAcceptableInput())
        list.append(this->ui->portEdit->text());
    else
        flag += 2;

    list[0] = QString::number(flag);

    return list;
}

void IPdialog::setDefault()
{
    ui->portEdit->setText(defaultPort);
}
