#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QListWidgetItem>
#include <QCloseEvent>
#include <QTimer>
#include <QInputDialog>
#include "ipdialog/ipdialog.h"
#include "database.h"
#include "validator.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

#define STATTIMEOUT 5000

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QTimer *syncer = nullptr;   //timer of syncronization

    Database *DB = nullptr;
    uint curID;                 //id of choosen record
    bool genderChangeIgnore;    //flag to ignore attempt to change gender if changed by program
    bool rowChangedIgnore;      //flag to ignore changing of current record if made by program

    bool checkStatus();             //checks status of database (true if ok)

    void showRecord(uint id);       //display choosen record in widgets
    int  saveRecord(uint id);       //saving widgets in choosen record

    Record formRecord();            //forms record with current content of widgets

    void setParentLists(Record rec);        //sets lists of parents avialable for given record
    void checkForParents();                 //check if peoples in other records can be parents of current
    bool isUnique(uint id, QString name);   //check if choosen name unique

    void enableButtons(bool b);             //disables(0) or enables(1) buttons

    void updateBrowser(int pos);            //updates browser moving selected line to correct position
    bool fillBrowser();                     //fills browser with contents of database
    QString bLineFromStruct(recLine rec);   //forms browser line from structure
    QString bLineFromRecord(Record &rec);   //forms browser line from record
    void updateLine(uint id);               //updates browser line of given record
    void updateChildren(uint id);           //updates browser lines of children of given record

    int findLongestName();                  //returns length of longest name
    int checkTabs(QString name, int max);   //how much tabs need to be added
    void resetTabs();                       //reinserts correct number of Tab's in every browser line
    void insertHeader(int t);               //inserts header to browser (t is number of tabs after "ФИО")
    int selectById(uint id);                //selects record in browser which has given id

    void closeEvent(QCloseEvent *event);    //handle closeEvent and ask if need to save

private slots:   
    void setDateRanges();               //sets min/max dates of birth/death according to all conditions
    void resetLimits();                 //refils lists of parents
    void genderChange();                //check if can change gender of current record
    void add();                         //adding record to array
    void saveR();                       //saving current record
    void open(QListWidgetItem *item);   //opens record choosen in browser
    void remove();                      //removes record from

    void sync();                        //syncronize contents of browser with database

    void onClear();     //clearing database
    void onFill();      //fills database with records
};
#endif // MAINWINDOW_H






